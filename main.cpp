#include "main.hpp" // for any includings
#include "misc.cpp" // miscellaneous
using namespace std;

bool isudp=false, istcp=true, isserver=false, isstrip=false, enforcestrip=false, iscompress=false, enforcecompress=false, isv4=true, isv6=false;
int debuglevel=0;
QString launchedas;

void argument_check(int argc, char *argv[]) {
  string current_argument;
  for (int i = 1; i < argc; ++i) {
    current_argument = argv[i];
    if      (current_argument == "-4" || current_argument ==           "--ipv4") { isv4=true; isv6=false; }
    else if (current_argument == "-6" || current_argument ==           "--ipv6") { isv6=true; isv4=false; }
    else if (current_argument == "-h" || current_argument ==           "--help") help(cvttostr(launchedas));
    else if (current_argument == "-V" || current_argument ==        "--version") version(cvttostr(launchedas));
    else if (current_argument == "-v" || current_argument ==        "--verbose") debuglevel++;
    else if (current_argument == "-d" || current_argument ==          "--debug") debuglevel++;
    else if (current_argument == "-S" || current_argument ==         "--silent") debuglevel=0;
    else if (current_argument == "-p" || current_argument ==           "--port") {
      // code part where if given <port> is a real integer
    }
    else if (current_argument == "-k" || current_argument ==         "--key-id") {
      // code part where gpg key id is verified to be vaild
    }
    else if (current_argument == "-u" || current_argument ==            "--udp") { isudp=true; istcp=false; }
    else if (current_argument == "-t" || current_argument ==            "--tcp") { istcp=true; isudp=false; }
    else if (current_argument == "-s" || current_argument ==          "--strip") isstrip=true;
    else if (current_argument == "-S" || current_argument ==       "--no-strip") { isstrip = false; enforcestrip = true; }
    else if (current_argument == "-c" || current_argument ==    "--compression") { 
      // code part where we check if there's any available compression methods
    }
    else if (current_argument == "-C" || current_argument == "--no-compression") { iscompress=false, enforcecompress=true; }
  }
}

int main(int argc, char *argv[]){
  launchedas = argv[0];
  if (argv[1] == NULL) {
    cerr << RED << "Didn't specify any address/argument!" << endl << "Leaving..." << endl << RESET;
    exit(1);
  }
  else argument_check(argc, argv);
}
