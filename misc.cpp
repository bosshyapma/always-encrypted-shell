#include <iostream>
#include <string>
#include <QByteArray>
#include <QString>
#include "main.hpp"
using namespace std;

string cvttostr(QString to_convert) { return to_convert.toStdString(); }
QString cvttoqstr(string to_convert) { return QString::fromStdString(to_convert); }

QByteArray encrypt(QByteArray to_encrypt){}
QByteArray decrypt(QByteArray to_decrypt){}

void version(std::string launchedas){
  cout << "Always Encrypted SHell v" << AEFS_VERSION << ", licensed under GNU GPLv3."            << endl <<
          "Launched program name (if helps): \"" << launchedas << "\"."                          << endl <<
          "Source code:              \"https://gitlab.com/bosshyapma/always-encrypted-shell/\"." << endl <<
          "License used (GNU GPLv3): \"https://www.gnu.org/licenses/gpl-3.0.en.html/\"."         << endl;
  exit(0);
}

void help(std::string launchedas){ 
  cout << "Always Encrypted SHell v" << AEFS_VERSION << " Help Text"                                                                        << endl <<
          "General options:"                                                                                                                << endl <<
          "  -4 / --ipv4                : Use IPv4 (\"012.345.678.901\"                         format) when connecting;"                   << endl <<
          "  -6 / --ipv6                : Use IPv6 (\"1234:5678:9ABC:DEF0:1234:5678:9ABC:DEF0\" format) when connecting;"                   << endl <<
          "  -h / --help                : Show this help message;"                                                                          << endl <<
          "  -V / --version             : Show version/license details (v" << AEFS_VERSION << " / GNU GPLv3);"                              << endl <<
          "  -v / --verbose             : Enable verbose mode and increase debug level each time specified (max 4);"                        << endl <<
          "                               0 = none (ok maybe some critical messages);"                                                      << endl <<
          "                               1 = local-side config details, version strings, connected ip address, server banner, ...;"        << endl <<
          "                               2 = remote-side config details, GPG key being verified, encryption ways to use, ...;"             << endl <<
          "                               3 = performance details, initial packet details, useless warnings, remote stats;"                 << endl <<
          "                     (EXTREME) 4 = all packet details, readable heart beats, performance details every 1 min, ...;"              << endl <<
          "                                   Use this ONLY when redirecting log to a file (or you will \"hate\" it);"                      << endl <<
          "  -d / --debug               : Alias for \"--verbose\";"                                                                         << endl <<
          "  -S / --silent              : Disable verbose mode and reset debug mode back to \"0 = none, only critical messages\";"          << endl <<
          "  -p / --port <port>         : Port to connect/open server at (default \"4000\");"                                               << endl <<
          "  -k / --key-id <gpg key id> : GPG key-pair ID to en/de-crypt packets."                                                          << endl <<
          "                               (generate one using \"gpg2 --full-generate-key\" or \"gpg --full-generate-key\");"                << endl <<
          "  -u / --udp                 : Use UDP when connecting to server/opening server;"                                                << endl <<
          "  -t / --tcp                 : Use TCP when connecting to server/opening server;"                                                << endl <<
          "  -s / --strip               : Strip (NOT COMPRESS!!!) the data being sent to the server to save bandwidth;"                     << endl <<
          "  -S / --no-strip            : Do not strip even if server says \"strip\"."                                                      << endl <<
          "                               (might not connect if server enforces \"stripping\");"                                            << endl <<
          "  -c / --compression         : Compress the data being sent to the server to save bandwidth;"                                    << endl <<
          "  -C / --no-compression      : Do not compress even if server says \"compress\"."                                                << endl <<
          "                               (might not connect if server enforces \"compression\");"                                          << endl <<
          ""                                                                                                                                << endl;
  exit(0);
}
