#define RESET         "\033[0m"  // for resetting to the original color
#define RED           "\033[31m" // error
#define GREEN         "\033[32m" // success
#define YELLOW        "\033[33m" // warning
#define BLUE          "\033[34m" // info
#define MAGENTA       "\033[35m" // command outputs
#define AEFS_VERSION 1 // define version as 1 (talks for itself, right?)

// QT stuff
#include <QCoreApplication>
#include <QByteArray>
#include <QString>

// C++ stuff
#include <iostream>
#include <string>
