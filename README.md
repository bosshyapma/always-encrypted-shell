# Always Encrypted SHell (AESH)
### What is this shell?
This shell is like **OpenSSH**, *providing secure encrypted communications between two untrusted hosts over an insecure network*[^1], in a *different* way.  
Instead of using **SSH** keys, AESH uses **GPG** keys and uses **QGpgMe** libs to achieve this.  

### Goals:
- [x] Can print out lines.
- [ ] Same as above but with colors.
- [ ] Can process arguments.
- [ ] Can parse given address.
- [ ] Can start a server/*mini-server*. [^2]
- [ ] Can connect to a server.
- [ ] Can make use of QGpgME (encryption/decryption).
- [ ] Can send/receive packets.

### Building from source:
##### Installing required packages (NOT COMPLETE):
###### Debian-Ubuntu (or anything that is **apt** based):
`apt-get install build-essential cmake libgpgmepp-dev`

##### Building:
```
git clone https://gitlab.com/bosshyapma/always-encrypted-shell.git
cd always-encrypted-shell
mkdir build && cd build/
cmake ..
make
```

Binary will be generated in where you ran **cmake/make** (or just at *\<project root\>/build/*

### How does server-client talks to each other?
- Client attempts to start a connection to server.  
- If connection is established, then they send their version strings.  
  If versions doesn't match (even a letter of commit hash), then client ends connection with warning "Versions doesn't match".  
- Server sends its public identification string.  
- Client compares received string with its own **known hosts**.  
  If they match, then they continue to talk.  
  If not, client will end connection immediately and warn user.  
  If the string is not found, then client asks user to add string to database.  
- Client sends a portion of the GPG public key (tell AESH with --key-id <key-id>).  
- Server has to respond with a different portion of the same key.  
  If server doesn't have public key, then it has to respond with one of the following:  
  1. send same portion that client sent.  
  2. send *0000...*.  
  3. reply with a message like "I don't know this public key.".  
- Client asks user for GPG private key passphrase (won't do that if key is passwordless)  
  Server will use passphrase cached from startup.  
- Client-Server starts sending all packets encrypted multiple times using private key  
  assuming that private key is installed on both sides because of matching public keys.  

### License:
AESH is licensed under [GNU General Public License v3 and/or later, click](https://www.gnu.org/licenses/gpl-3.0.en.html/) or see the `LICENSE` file in the root of this directory.

[^1]: Copied from `man sshd`.
[^2]: Used by client side to receive server packets on a random port.  
      Port that is specified will be included within the initial packet to the server.  
